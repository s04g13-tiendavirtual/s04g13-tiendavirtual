# importar módulo de PyMySQL
import pymysql


def conexion_db():

    # Configuración para conexión a base de datos
    HOST = 'db4free.net'
    PORT = 3306
    USER = 'misiontic_uis'
    PSW = 'misiontic.uis'
    DB = 'db_tiendaonline'

    conexion = pymysql.connect(host=HOST, 
                                port=PORT, 
                                user=USER,
                                passwd=PSW,
                                db=DB)
    
    return conexion