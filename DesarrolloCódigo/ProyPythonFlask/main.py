# Importar el módulo de flask
from flask import Flask, render_template, url_for, redirect, request
# Importar controlador de la base de datos
import controladordb


# utilizar variable para crear la aplicación
app = Flask(__name__)


# ruta inicial cuando abro el proyecto
@app.route('/') 
# la ruta está asociada a un método, en este caso el index
def index(rstDB = None):
    # conexion = conexion_db()
    if rstDB != None:
        rstDB = "Valor diferente a None"
    else:
        rstDB = controladordb.leer_datos()
    #     rstDB = conexion
    #     # rstDB = "valor"
    #     cursor = conexion.cursor()
    #     sql = "SELECT * FROM productos"
    #     cursor.execute(sql)
    #     # rstDB = cursor.fetchone()
    #     rstDB = []
    #     rstDB = cursor.fetchall()

    # cursor.close()
    # return "<h1> Hola Tripulantes... </h1>"
    return render_template('index.html', 
                            rstDB = rstDB)


@app.route('/registrar_productos')
def registrar_productos():
    return render_template('registrar_productos.html')

@app.route('/guardar_datos', methods = ['GET', 'POST'])
def guardar_datos():
    if request.method == 'POST':
        nombre = request.form['nombre']
        cantidad = request.form['cantidad']
        categoria = request.form['categoria']
        precio = request.form['precio']
        rstDB = controladordb.insertar_datos(nombre,
                cantidad, categoria, precio)
        # return f"<h1>{rstDB}</h1>"
        return render_template('guardar_datos.html',
                                rstDB = rstDB)
    return redirect(url_for('index'))

@app.route('/editar_productos', methods = ['GET', 'POST'])
def editar_productos():
    if request.method == 'POST':
        id = request.form['id']
        rstDB = controladordb.leer_datos_id(id)
        return render_template('editar_productos.html',
                                rstDB = rstDB)
    return redirect(url_for('index'))

@app.route('/actualizar_datos', methods = ['GET', 'POST'])
def actualizar_datos():
    if request.method == 'POST':
        id = request.form['id']
        nombre = request.form['nombre']
        cantidad = request.form['cantidad']
        categoria = request.form['categoria']
        precio = request.form['precio']
        rstDB = controladordb.actualizar_datos(id, nombre, cantidad, categoria, precio)
        # return f"<h1>{rstDB}</h1>"
        return render_template('actualizar_datos.html',
                                rstDB = rstDB)
    return redirect(url_for('index'))

@app.route('/confirmar_eliminar_dato', methods = ['GET', 'POST'])
def confirmar_eliminar_dato():
    if request.method == 'POST':
        id = request.form['id']
        rstDB = controladordb.leer_datos_id(id)
        return render_template('confirmar_eliminar_dato.html',
                                rstDB = rstDB)
    return redirect(url_for('index'))

@app.route('/eliminar_dato', methods = ['GET', 'POST'])
def eliminar_dato():
    if request.method == 'POST':
        id = request.form['id']
        rstDB = controladordb.eliminar_dato(id)
        # return f"<h1>{rstDB}</h1>"
        return render_template('eliminar_dato.html',
                                rstDB = rstDB)
    return redirect(url_for('index'))



@app.route('/saludo')
def saludo():
    # return "<h1> Programando ando... </h1>"
    return render_template('saludo.html')

@app.route('/informacion')
def informacion():
    return render_template('informacion.html')

@app.route('/contacto')
@app.route('/contacto/<email>')
def contacto(email = None):
    
    if email != None:
        email = "misiontic.formador17@uis.edu.co"
    else:
        email = "No disponble"
        
    # return f"""
    # <h1> Correo Electrónico Contacto </h1>
    # <h2> E-mail: {email} </h2>
    # """
    # return render_template('contacto.html'), email
    return render_template('contacto.html', 
                            email = email,
                            num = 10)

    # ###### Revisas Paso Variable #########


# Condicional que me permite identificar el fichero principal.
if __name__ == '__main__':
    # app.run(debug=True)
    app.run(debug=False)