# Importar conexión base de datos
from conexiondb import conexion_db

# CRUD => Create - Read - Update - Delete

# Create =>
def insertar_datos(nombre, cantidad, categoria, precio):
    conexion = conexion_db()
    with conexion.cursor() as cursor:
        # sql = "INSERT INTO productos(id, nombre, cantidad, categoria, precio) VALUES(NULL, 'Costillitas', 60, 'Carnes', 16000)"
        sql = f"INSERT INTO productos(id, nombre, cantidad, categoria, precio) VALUES(NULL, '{nombre}', {cantidad}, '{categoria}', {precio})"
        cursor.execute(sql)
    conexion.commit()
    conexion.close()
    return "Datos Guardados"

# Read =>
# lee todos los datos de la tabla
def leer_datos():
    conexion = conexion_db()
    # cursor = conexion.cursor()
    rstDB = []
    with conexion.cursor() as cursor:
        sql = "SELECT * FROM productos"
        cursor.execute(sql)
        # rstDB = []
        rstDB = cursor.fetchall()
    cursor.close()
    return rstDB

# lee los datos de la tabla, especificando un id
def leer_datos_id(id):
    conexion = conexion_db()
    # cursor = conexion.cursor()
    rstDB = []
    with conexion.cursor() as cursor:
        sql = f"SELECT * FROM productos WHERE id = {id}"
        cursor.execute(sql)
        # rstDB = []
        # rstDB = cursor.fetchall()
        rstDB = cursor.fetchone()
    cursor.close()
    return rstDB


# Update =>
def actualizar_datos(id, nombre, cantidad, categoria, precio):
    conexion = conexion_db()
    with conexion.cursor() as cursor:
        # id = 15
        # sql = f"UPDATE productos SET nombre = 'Gaseosa', cantidad = 160, categoria = 'Golosinas', precio = 3000 WHERE id = {id}"
        sql = f"UPDATE productos SET nombre = '{nombre}', cantidad = {cantidad}, categoria = '{categoria}', precio = {precio} WHERE id = {id}"
        cursor.execute(sql)
        conexion.commit()
    cursor.close()
    return "Datos actualizados..."


# Delete =>
def eliminar_dato(id):
    conexion = conexion_db()
    with conexion.cursor() as cursor:
        # id = 15
        sql = f"DELETE FROM productos WHERE id = {id}"
        cursor.execute(sql)
        conexion.commit()
    cursor.close()
    return "Datos eliminados..."
