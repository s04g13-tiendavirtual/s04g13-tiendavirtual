<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title>JSP - Hello World</title>
    </head>
    <body>
        <br>
        <h1>
            <%= "Hello World!" %>
        </h1>

        <br>
        <h2>
            Hola Tripulantes...
        </h2>
        <br/>
        <a href="MostrarProductos">Click Aquí para Mostrar Productos</a>
        <p>
            HTML (Lenguaje de Marcas de Hipertexto, del inglés HyperText Markup Language)
            es el componente más básico de la Web. Define el significado y la estructura
            del contenido web.
        </p>

        <p>
            url: jdbc:mysql://db4free.net:3306/db_tiendaonline <br>
            user: misiontic_uis <br>
            psw: misiontic.uis <br>
            nombre_db: db_tiendaonline <br>

            DBMS: MySQL (ver. 8.0.26) <br>
            Case sensitivity: plain=exact, delimited=exact <br>
            Driver: MySQL Connector/J (ver. mysql-connector-java-8.0.25 (Revision: 08be9e9b4cba6aa115f9b27b215887af40b159e0), JDBC4.2)
            <br>
            Ping: 532 ms <br>
            SSL: yes
        </p>
        <p>
            Otro párrafo... programando ando...
        </p>
        <br/>
        <a href="hello-servlet">Hello Servlet</a>
        <br>
<%--        <a href="mostrar-contenido"> Click aquí para mostrar contenido </a>--%>
    </body>
</html>