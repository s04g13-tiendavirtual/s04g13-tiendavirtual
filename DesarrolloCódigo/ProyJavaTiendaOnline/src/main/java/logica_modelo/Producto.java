package logica_modelo;
//package conexionDB;

import persistencia_controlador.ConexionDB;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class Producto {

    // Atributos
    private int id;
    private String nombre;
    private int cantidad;
    private String categoria;
    private double precio;

    // Constructor por defecto sin parámetros
    public Producto(){}

    // Constructor con parámetros
    public Producto(String nombre, int cantidad, String categoria, double precio){
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.categoria = categoria;
        this.precio = precio;
    }

    public int getId(){
        return this.id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getNombre(){
        return this.nombre;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public int getCantidad(){
        return this.cantidad;
    }

    public void setCantidad(int cantidad){
        this.cantidad = cantidad;
    }

    public String getCategoria(){
        return this.categoria;
    }

    public void setCategoria(String categoria){
        this.categoria = categoria;
    }

    public double getPrecio(){
        return this.precio;
    }

    public void setPrecio(double precio){
        this.precio = precio;
    }

    //no elimine ni modifique este metodo
    @Override
    public String toString() {
        return "Producto{" + "id=" + id + ", nombre=" + nombre + ", cantidad=" + cantidad + ", categoria=" + categoria + ", precio=" + precio + '}';
    }

    // CRUD => Create Read Update Delete
    // Create => insertar o guarda productos en bd
    public boolean guardarBD(){
        ConexionDB cnx = new ConexionDB();
        // INSERT INTO productos(nombre,cantidad,categoria,precio)
        // VALUES('Costillas', 3, 'Carnes', 16000.0);
        String sql = "INSERT INTO productos(nombre, cantidad, categoria, precio)" +
                " VALUES('" + nombre + "'," + cantidad + ",'" + categoria +
                "'," + precio + ");";
        if (cnx.setAutoCommitBD(false)){ // valida no autogardado
            if (cnx.insertarBD(sql)){
                cnx.commitBD(); // confirmando guardar en la bd.
                cnx.cerrarConexion();
                return true;
            }
            else {
                cnx.rollbackBD();
                cnx.cerrarConexion();
                return false;
            }
        }
        else {
            cnx.cerrarConexion();
            return false;
        }
    }

    // Read => SELECT ver los datos de la tabla
    public List<Producto> listarProductos(){
        List<Producto> listaProductos = new ArrayList<>();
        ConexionDB cnx = new ConexionDB();
        String sql = "SELECT * FROM productos;";
        try {
            ResultSet rs = cnx.consultarBD(sql);
            Producto p;
            while (rs.next()){
                p = new Producto();
                p.setId(rs.getInt("id"));
                p.setNombre(rs.getString("nombre"));
                p.setCantidad(rs.getInt("cantidad"));
                p.setCategoria(rs.getString("categoria"));
                p.setPrecio(rs.getDouble("precio"));
                listaProductos.add(p);
            }

        } catch (SQLException e){
            System.out.println("Error en lista de produectos "+ e.getMessage());
        } finally {
            cnx.cerrarConexion();
        }
        return listaProductos;
    }

    // Update => UPDATE actualizar, modificar, corregir datos de la db
    public boolean actualizarProducto(){
        ConexionDB conexionBD = new ConexionDB();
        // UPDATE productos SET nombre='Costillas', cantidad=6, categoria='Carnes', precio=13000 WHERE id=6;
        String sql = "UPDATE productos SET nombre='" + nombre +
                "', cantidad=" + cantidad + ", categoria='" + categoria +
                "', precio=" + precio + " WHERE id=" + id + ";";
        if (conexionBD.setAutoCommitBD(false)){
            if (conexionBD.actualizarBD(sql)){
                conexionBD.commitBD();
                conexionBD.cerrarConexion();
                return true;
            }
            else {
                conexionBD.rollbackBD();
                conexionBD.cerrarConexion();
                return false;
            }
        }
        else {
            conexionBD.cerrarConexion();
            return false;
        }
    }

    // Delete => DELETE eliminar, remover productos de bd.
    public boolean eliminarProducto(){
        ConexionDB conexionBD = new ConexionDB();
        // DELETE FROM productos WHERE id=4;
        String sql = "DELETE FROM productos WHERE id=" + id + ";";
        // delete from productos where id>7;
        //String sql = "delete from productos where id>" + id + ";";
        if (conexionBD.setAutoCommitBD(false)){
            if (conexionBD.barrarBD(sql)){
                conexionBD.commitBD();
                conexionBD.cerrarConexion();
                return true;
            }
            else {
                conexionBD.rollbackBD();
                conexionBD.cerrarConexion();
                return false;
            }
        }
        else {
            conexionBD.cerrarConexion();
            return false;
        }
    }

}



// -------------------------------------------------------------
//package otrasPaginasWebConJava;
////package conexionDB;
//
//        import conexion_db.ConexionDB;
//        import jakarta.servlet.annotation.WebServlet;
//        import jakarta.servlet.http.HttpServlet;
//        import jakarta.servlet.http.HttpServletRequest;
//        import jakarta.servlet.http.HttpServletResponse;
//
//        import java.io.IOException;
//        import java.io.PrintWriter;
//
//@WebServlet(name = "mostrarContenido", value = "/mostrar-contenido")
//public class MostrarContenido extends HttpServlet {
//    public void init(){
//    }
//
//    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//        resp.setContentType("text/html");
//
//        PrintWriter imprimir = resp.getWriter();
//        imprimir.println("<html><body>");
//        imprimir.println("<h1> Hola Tripulantes </h1>");
//        imprimir.println("<p> Contenido en otra página con un párrafo... </p>");
//        double suma = ConexionDB.sumaAB(10, 6);
//        imprimir.println("<h2>" + "El resultado de a + b es = " + suma + "</h2>");
//        imprimir.println("</body></html>");
//    }
//
//    public void destroy(){
//
//    }
//}
