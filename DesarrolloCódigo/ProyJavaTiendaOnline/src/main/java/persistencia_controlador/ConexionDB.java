package persistencia_controlador;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConexionDB {

    // Atributos
    private String url = "";
    private String host = "";
    private int port;
    private String db = "";
    private String user = "";
    private String psw = "";
    private Connection con = null;
    private Statement stmt = null;
    private ResultSet rs = null;

    // Constructor
    public ConexionDB(){
        // url = "jdbc:sqlite:reto5db.db";
        // host = "db4free.net";
        host = "localhost";
        port = 3306;
        db = "db_tiendaonline";
        user = "misiontic_uis";
        psw = "misiontic.uis";
        url = "jdbc:mysql://" + host + ":" + port + "/" + db;
        // url = "jdbc:mysql://db4free.net:3306/db_tiendaonline";
        try {
            // con = DriverManager.getConnection(url);
            con = DriverManager.getConnection(url, user, psw);
            if (con != null){
                //con = null;
                DatabaseMetaData meta = con.getMetaData();
                System.out.println("Conexión establecida: ");
                System.out.println(meta.getDriverName());
            }
        } catch (SQLException ex){
            System.out.println(ex.getMessage());
            System.out.println("Conexión Errónea");
        }
    }


    // Retorna la conexión
    public Connection getConnection(){
        return con;
    }

    // Cierra la conexión
    public void  closeConnection(Connection con){
        if (con != null){
            try {
                con.close();
            } catch (SQLException e){
                Logger.getLogger(ConexionDB.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    // Realizar consultas a la db => SELECT
    public ResultSet consultarBD(String sentencia){
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(sentencia);
        } catch (SQLException sqlex){
            System.out.println(sqlex.getMessage());
        } catch (RuntimeException rex){
            System.out.println(rex.getMessage());
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        return rs;
    }

    // Insertar datos a db  => INSERT
    public boolean insertarBD(String sentencia){
        try {
            stmt = con.createStatement();
            stmt.execute(sentencia);
        }catch (SQLException | RuntimeException e){
            System.out.println("ERROR RUTINA: "+ e);
            return false;
        }
        return true;
    }

    // Borrar datos de la db  => DELETE
    public boolean barrarBD(String sentencia){
        try {
            stmt = con.createStatement();
            stmt.execute(sentencia);
        }catch (SQLException | RuntimeException e){
            System.out.println("ERROR RUTINA: " + e);
            return false;
        }
        return true;
    }

    // Actualizar datos de bd => UPDATE
    public boolean actualizarBD(String sentencia){
        try {
            stmt = con.createStatement();
            stmt.executeUpdate(sentencia);
        }catch (SQLException | RuntimeException e){
            System.out.println("ERROR RUTINA: " + e);
            return false;
        }
        return true;
    }

    // Auto guardado
    public boolean setAutoCommitBD(boolean bol){
        try {
            con.setAutoCommit(bol);
        } catch (SQLException e){
            System.out.println("Error de configuración autoCommit " + e.getMessage());
            return false;
        }
        return true;
    }

    // cerrar conexión ya hay una función closeConnection()
    public void cerrarConexion(){
        closeConnection(con);
    }

    // confirmar guardar sentencia
    public boolean commitBD(){
        try {
            con.commit();
            return true;
        } catch (SQLException e){
            System.out.println("Error en commit " + e.getMessage());
            return false;
        }
    }

    // cancelar sentencia
    public boolean rollbackBD(){
        try {
            con.rollback();
            return true;
        } catch (SQLException e){
            System.out.println("Error en rollback " + e.getMessage());
            return false;
        }
    }





//   Función anterior probando el llamado desde otra clase
//    public static double sumaAB(double a, double b){
//        return a + b;
//    }



}
