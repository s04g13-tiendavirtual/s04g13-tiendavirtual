package presentacion_vista;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import logica_modelo.Producto;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "MostrarProductos", value = "/MostrarProductos")
public class MostrarProductos extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();

        out.println("<html><body>");
        out.println("<h1>Productos Registrados en la Tienda Online </h1>");

        Producto prod = new Producto();
        List<Producto> listaProd = prod.listarProductos();
        System.out.println("lista : " + listaProd);

        for (Producto p : listaProd) {
            out.println("<p>" + p.getId() + "</p>");
            out.println("<p>" + p.getNombre() + "</p>");
            out.println("<p>" + p.getCantidad() + "</p>");
            out.println("<p>" + p.getCategoria() + "</p>");
            out.println("<p>" + p.getPrecio() + "</p>");
        }

        out.println(" </body> </html>");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
